#include <ros/ros.h>
#include <std_srvs/Empty.h>

bool forward = false;
bool toggle_forward(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp)
{
	forward =! forward;
	ROS_INFO_STREAM("Now sending");
	return true;
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "hello");
	
	ros::NodeHandle nh;
	
	ros::ServiceServer server = nh.advertiseService("toggle", toggle_forward);
	
	while(ros::ok())
	{
		if(forward)
			ROS_INFO_STREAM("hello buddy");
		else
			ROS_INFO_STREAM("nothing yet buddy");
	
		ros::spinOnce();
	}
}
	
	
	
	
	
	
