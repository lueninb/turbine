
/** Testing basic takeoff, flight path and landing
 * May 30, 2020 
 * 
 */

//INCLUDE
#include <ros/ros.h>
#include <dji_osdk_ros/common_type.h>

#include <dji_osdk_ros/FlightTaskControl.h>
#include <dji_osdk_ros/SetGoHomeAltitude.h>
#include <dji_osdk_ros/SetNewHomePoint.h>

#include <turbine_segmentation/BladeAngles.h>
#include <turbine_segmentation/BladePath.h>

#include <turbine_segmentation/BladeTrackingAngle.h>
#include <turbine_segmentation/BladeTrackingCommand.h>

#include <iostream>
#include <stdlib.h>


//CODE
using namespace dji_osdk_ros;

ros::ServiceClient task_control_client;

bool moveByPosOffset(FlightTaskControl& task, MoveOffset&& move_offset);

bool startBladeTracking = false;
float currentBladeAngle = 0;
int count = 0;
bool toggleBladeTracking(turbine_segmentation::BladeTrackingAngle::Request &req, turbine_segmentation::BladeTrackingAngle::Response &resp)
{
	startBladeTracking =! startBladeTracking;
	currentBladeAngle = req.bladeAngle;
	resp.result = 1;
	count++;
	ROS_INFO_STREAM("Now sending.....................................");
	return true;
}

int main(int argc, char** argv)
{

	ros::init(argc, argv, "blade_flight_node");
	ros::NodeHandle nh;
	task_control_client = nh.serviceClient<FlightTaskControl>("/flight_task_control");

	ros::ServiceServer server = nh.advertiseService("toggleBladeTracking", toggleBladeTracking);
	ros::ServiceClient client = nh.serviceClient<turbine_segmentation::BladeTrackingCommand>("drone_commands");

	FlightTaskControl control_task;

	//control_task.request.task = FlightTaskControl::Request::TASK_TAKEOFF;
	//ROS_INFO_STREAM("Takeoff request sending ...");
	//task_control_client.call(control_task);
	
	//if(control_task.response.result == false)
	//	ROS_ERROR_STREAM("Takeoff task failed");


	//if(control_task.response.result == true)
	//{
		//ROS_INFO_STREAM("Takeoff task successful");
		//ros::Duration(2.0).sleep();

	while(ros::ok())
	{	
		if (startBladeTracking)
		{
			//ROS_INFO_STREAM("Okay ...");
			turbine_segmentation::BladeTrackingCommand srv;
			srv.request.initBladeAngle = currentBladeAngle;
			client.call(srv);
			//std::cout << "\nbladeX " << srv.response.xCmd << " \nbladeY " << srv.response.yCmd << "\nbladeZ " << srv.response.zCmd << std::endl;
			
			ROS_INFO_STREAM("Move by position offset request sending ...");
			moveByPosOffset(control_task, MoveOffset(srv.response.xCmd, srv.response.yCmd, srv.response.zCmd, 0.0));
			ROS_INFO_STREAM("Move over!");
			
			
			//ros::Duration(4.0).sleep();
		}
		//else
		//	ROS_INFO_STREAM("Nothing...");
		
		if (count >= 2)
			break;
			
		ros::spinOnce();
	}

	ROS_INFO_STREAM("Complete");


		//control_task.request.task = FlightTaskControl::Request::TASK_LAND;
		//ROS_INFO_STREAM("Landing request sending ...");
		//task_control_client.call(control_task);
	  
		//if(control_task.response.result == true)
		//	ROS_INFO_STREAM("Land task successful");
		

	//}
	
	//Gracefully kill the rosbag record node
	ros::Duration(4.0).sleep();
	int stopBagfile = system("rosnode kill /rosbag_record_flight");
	
	//ros::spin();
	return 0;
	}


bool moveByPosOffset(FlightTaskControl& task, MoveOffset&& move_offset)
{
  task.request.task = FlightTaskControl::Request::TASK_GO_LOCAL_POS;
  // pos_offset: A vector contains that position_x_offset, position_y_offset, position_z_offset in order
  task.request.pos_offset.clear();
  task.request.pos_offset.push_back(move_offset.x);
  task.request.pos_offset.push_back(move_offset.y);
  task.request.pos_offset.push_back(move_offset.z);
  // yaw_params: A vector contains that yaw_desired, position_threshold(Meter), yaw_threshold(Degree)
  task.request.yaw_params.clear();
  task.request.yaw_params.push_back(move_offset.yaw);
  task.request.yaw_params.push_back(move_offset.pos_threshold);
  task.request.yaw_params.push_back(move_offset.yaw_threshold);
  task_control_client.call(task);
  return task.response.result;
}
