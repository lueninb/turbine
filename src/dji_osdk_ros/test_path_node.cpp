
/** Testing basic takeoff, flight path and landing
 * May 30, 2020 
 * 
 */

//INCLUDE
#include <ros/ros.h>
#include <dji_osdk_ros/common_type.h>

#include <dji_osdk_ros/FlightTaskControl.h>
#include <dji_osdk_ros/SetGoHomeAltitude.h>
#include <dji_osdk_ros/SetNewHomePoint.h>

#include <std_srvs/Empty.h>

#include <stdlib.h>


//CODE
using namespace dji_osdk_ros;

ros::ServiceClient task_control_client;
//ros::ServiceClient say_hello_client;

bool moveByPosOffset(FlightTaskControl& task, MoveOffset&& move_offset);


int main(int argc, char** argv)
{
	//int recordBagfile = system("rosbag record -o flight /livox/lidar __name:=my_bag");

	ros::init(argc, argv, "test_path_node");
	ros::NodeHandle nh;
	task_control_client = nh.serviceClient<FlightTaskControl>("/flight_task_control");
	//say_hello_client = nh.serviceClient<std_srvs::Empty>("/toggle");

	FlightTaskControl control_task;

	control_task.request.task = FlightTaskControl::Request::TASK_TAKEOFF;
	ROS_INFO_STREAM("Takeoff request sending ...");
	task_control_client.call(control_task);
	
	//std_srvs::Empty::Request req;
	//std_srvs::Empty::Request resp;
	
	//bool success = say_hello_client.call(req, resp);
	

	if(control_task.response.result == false)
		ROS_ERROR_STREAM("Takeoff task failed");


	if(control_task.response.result == true)
	{
		ROS_INFO_STREAM("Takeoff task successful");
		ros::Duration(2.0).sleep();

		ROS_INFO_STREAM("Move by position offset request sending ...");
		moveByPosOffset(control_task, MoveOffset(0.0, 0.0, 5.0, 0.0));
		ROS_INFO_STREAM("Step 1 over!");

		moveByPosOffset(control_task, MoveOffset(0.0, 15.0, 0.0, 0.0));
		ROS_INFO_STREAM("Step 2 over!");

		moveByPosOffset(control_task, MoveOffset(0.0, -15.0, 0.0, 0.0));
		ROS_INFO_STREAM("Step 3 over!");
/*		
		moveByPosOffset(control_task, MoveOffset(0.0, -5.0, 0.0, 0.0));
		ROS_INFO_STREAM("Step 4 over!");
		
		moveByPosOffset(control_task, MoveOffset(0.0, 0.0, 0.0, 90.0));
		ROS_INFO_STREAM("Step 5 over!");
		
		moveByPosOffset(control_task, MoveOffset(0.0, 0.0, 0.0, -90.0));
		ROS_INFO_STREAM("Step 6 over!");
*/
		control_task.request.task = FlightTaskControl::Request::TASK_LAND;
		ROS_INFO_STREAM("Landing request sending ...");
		task_control_client.call(control_task);
	  
		if(control_task.response.result == true)
			ROS_INFO_STREAM("Land task successful");
		

	}
 
	ROS_INFO_STREAM("Complete");
	
	//Gracefully kill the rosbag record node
	ros::Duration(4.0).sleep();
	int stopBagfile = system("rosnode kill /rosbag_record_lidar");
	
	ros::spin();
	return 0;
	}


bool moveByPosOffset(FlightTaskControl& task, MoveOffset&& move_offset)
{
  task.request.task = FlightTaskControl::Request::TASK_GO_LOCAL_POS;
  // pos_offset: A vector contains that position_x_offset, position_y_offset, position_z_offset in order
  task.request.pos_offset.clear();
  task.request.pos_offset.push_back(move_offset.x);
  task.request.pos_offset.push_back(move_offset.y);
  task.request.pos_offset.push_back(move_offset.z);
  // yaw_params: A vector contains that yaw_desired, position_threshold(Meter), yaw_threshold(Degree)
  task.request.yaw_params.clear();
  task.request.yaw_params.push_back(move_offset.yaw);
  task.request.yaw_params.push_back(move_offset.pos_threshold);
  task.request.yaw_params.push_back(move_offset.yaw_threshold);
  task_control_client.call(task);
  return task.response.result;
}
